package controle;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import modelo.Code;

public class CodeController {
    public boolean insert(Code code) {
	boolean boolReturn = false;
	Connection con = new Connection();
	try {
	    String query = "INSERT INTO codes(author, lang, project, creation_date) VALUES(?, ?, ?, ?);";
	    PreparedStatement comand = con.getCon().prepareStatement(query);
	    comand.setString(1, code.getAuthor());
	    comand.setString(2, code.getLang());
	    comand.setString(3, code.getProject());
	    String creationDateStr = code.creationDate.getYear() + "-" + code.creationDate.getMonth() + "-"
		    + code.creationDate.getDate();
	    Date creationDateSQL = Date.valueOf(creationDateStr);
	    comand.setDate(4, creationDateSQL);
	    if (!comand.execute()) {
		boolReturn = true;
	    }
	} catch (SQLException sqlEx) {
	    System.out.println("[FAILED] Erro ao inserir código:" + sqlEx.getMessage());
	} finally {
	    con.closeCon();
	}
	return boolReturn;
    }

    public boolean update(Code code) {
	boolean boolReturn = false;
	Connection con = new Connection();
	try {
	    String query = "UPDATE codes SET author = ?, lang = ?, project = ?, creation_date = ? WHERE id = ?;";
	    PreparedStatement comand = con.getCon().prepareStatement(query);
	    comand.setString(1, code.getAuthor());
	    comand.setString(2, code.getLang());
	    comand.setString(3, code.getProject());
	    String creationDateStr = code.creationDate.getYear() + "-" + code.creationDate.getMonth() + "-"
		    + code.creationDate.getDate();
	    Date creationDateSQL = Date.valueOf(creationDateStr);
	    comand.setDate(4, creationDateSQL);
	    comand.setInt(5, code.getId());
	    if (!comand.execute()) {
		boolReturn = true;
	    }
	    comand.setDate(4, creationDateSQL);
	} catch (SQLException sqlEx) {
	    System.out.println("[FAILED] Erro ao atualizar registro" + sqlEx.getMessage());
	} finally {
	    con.closeCon();
	}
	return boolReturn;
    }

    public boolean delete(int id) {
	boolean boolReturn = false;
	Connection con = new Connection();
	try {
	    String query = "DELETE FROM codes WHERE id = ?;";
	    PreparedStatement comand = con.getCon().prepareStatement(query);
	    comand.setInt(1, id);
	    if (!comand.execute()) {
		boolReturn = true;
	    }
	} catch (SQLException sqlEx) {
	    System.out.println("[FAILED] Erro ao remover código" + sqlEx.getMessage());
	} finally {
	    con.closeCon();
	}
	return boolReturn;
    }
}
