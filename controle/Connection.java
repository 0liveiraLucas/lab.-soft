package controle;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Connection {
    private java.sql.Connection con = null;

    public Connection() {
	try {
	    String user = "admin", pwd = "123456", driver = "mysql", host = "localhost", dbname = "aulas";
	    Class.forName("com.mysql.cj.jdbc.Driver");
	    String server = String.format("jdbc:%s://%s/%s", driver, host, dbname);
	    this.con = DriverManager.getConnection(server, user, pwd);
	    System.out.println("[  OK  ] Conexão bem sucedida");
	} catch (ClassNotFoundException cEx) {
	    System.out.println("[FAILED] Erro interno");
	} catch (SQLException sqlEx) {
	    System.out.println("[FAILED] Erro no banco de dados");
	} catch (Exception ex) {
	    System.out.println("[FAILED] Erro geral");
	}
    }
    
    public java.sql.Connection getCon() {
	return this.con;
    }
    
    public void closeCon() {
	try {
	    this.con.close();
	}catch(SQLException sqlEx) {
	    System.out.println("[FAILED] Erro ao encerrar conexão");
	}
    }
}
