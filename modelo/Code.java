package modelo;

import java.util.Date;

public class Code {
    private int id;
    private String author;
    private String lang;
    private String project;
    public Date creationDate = new Date();
    public void setId(int id) {
        this.id = id;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getLang() {
        return lang;
    }
    public void setLang(String lang) {
        this.lang = lang;
    }
    public String getProject() {
        return project;
    }
    public void setProject(String project) {
        this.project = project;
    }
    public int getId() {
        return id;
    }
    
}
