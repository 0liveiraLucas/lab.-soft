import controle.CodeController;
import modelo.Code;

public class App {
    public static void main(String[] args) {
	Code code = new Code();
        CodeController controller = new CodeController();
        code.setId(1);
        code.setAuthor("Palpite");
        code.setLang("D");
        code.setProject("Death Star");
        code.creationDate.setDate(1);
        code.creationDate.setMonth(1);
        code.creationDate.setYear(1970);
        
        if(controller.update(code) && controller.delete(1)) {
            System.out.println("[  OK  ] Comando bem sucedido");
        } else {
            System.out.println("[FAILED] Comando mal sucedido");
        }
    }
}
